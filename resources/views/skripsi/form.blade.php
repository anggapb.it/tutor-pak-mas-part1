<x-app-layout>
    <x-slot name="header">
        <p class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Form Pengajuan Skripsi') }}
        </p>
    </x-slot>

    <div class="mt-10 sm:mt-0">
          <div class="mt-5 md:mt-0 md:col-span-2">
            <form action="{{ url("/pengajuan-skripsi-store") }}" method="POST">
                @csrf
              <div class="shadow overflow-hidden sm:rounded-md">
                <div class="px-4 py-5 bg-white sm:p-6">
                  <div class="grid grid-cols-6 gap-6">
                    <div class="col-span-6 sm:col-span-3">
                        <label for="mahasiswa_id" class="block text-sm font-medium text-gray-700">Mahasiswa</label>
                        <select id="mahasiswa_id" name="mahasiswa_id" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" required>
                            <option value="">Silahkan Pilih...</option>
                            @foreach ($mahasiswa as $mhs)
                         <option value="{{ $mhs->id }}">{{ $mhs->nim.' - '.$mhs->nama_mahasiswa }}</option>
                         @endforeach
                        </select>
                      </div>

                    <div class="col-span-6 sm:col-span-3">
                      <label for="keahlian" class="block text-sm font-medium text-gray-700">Kelompok Keahlian</label>
                      <select id="keahlian" name="keahlian" autocomplete="keahlian-name" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                        <option value="0">Silahkan Pilih...</option>
                        <option value="Keahlian 1">Kelahlian 1</option>
                        <option value="Keahlian 2">Kelahlian 2</option>
                        <option value="Keahlian 3">Kelahlian 3</option>
                      </select>
                    </div>

                    <div class="col-span-6">
                      <label for="ipk" class="block text-sm font-medium text-gray-700">IPK</label>
                      <input type="text" name="ipk" id="ipk" autocomplete="ipk" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                    </div>

                    <div class="col-span-6 sm:col-span-6 lg:col-span-2">
                      <label for="judul_skripsi" class="block text-sm font-medium text-gray-700">Judul Skripsi</label>
                      <input type="text" name="judul_skripsi" id="judul_skripsi" autocomplete="address-level2" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                    </div>

                    <div class="col-span-6 sm:col-span-3">
                        <label for="dosenwali_id" class="block text-sm font-medium text-gray-700">Dosen wali</label>
                        <select id="dosenwali_id" name="dosenwali_id" required class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                            <option value="">Silahkan Pilih...</option>
                            @foreach ($dosen as $row)
                         <option value="{{ $row->id }}">{{ $row->nama_dosen }}</option>
                         @endforeach
                        </select>
                      </div>
                    <div class="col-span-6 sm:col-span-3">
                        <label for="dosenkonsul1_id" class="block text-sm font-medium text-gray-700">Dosen Konsultan 1</label>
                        <select id="dosenkonsul1_id" name="dosenkonsul1_id" required class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                            <option value="">Silahkan Pilih...</option>
                            @foreach ($dosen as $row)
                         <option value="{{ $row->id }}">{{ $row->nama_dosen }}</option>
                         @endforeach
                        </select>
                      </div>
                    <div class="col-span-6 sm:col-span-3">
                        <label for="dosenkonsul2_id" class="block text-sm font-medium text-gray-700">Dosen Konsultan 2</label>
                        <select id="dosenkonsul2_id" name="dosenkonsul2_id" required class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                            <option value="">Silahkan Pilih...</option>
                            @foreach ($dosen as $row)
                         <option value="{{ $row->id }}">{{ $row->nama_dosen }}</option>
                         @endforeach
                        </select>
                      </div>
                    <div class="col-span-6 sm:col-span-3">
                        <label for="dospem1_id" class="block text-sm font-medium text-gray-700">Dosen Pembimbing 1</label>
                        <select id="dospem1_id" name="dospem1_id" required class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                            <option value="">Silahkan Pilih...</option>
                            @foreach ($dosen as $row)
                         <option value="{{ $row->id }}">{{ $row->nama_dosen }}</option>
                         @endforeach
                        </select>
                      </div>
                    <div class="col-span-6 sm:col-span-3">
                        <label for="dospem2_id" class="block text-sm font-medium text-gray-700">Dosen Pembimbing 2</label>
                        <select id="dospem2_id" name="dospem2_id" required class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                            <option value="">Silahkan Pilih...</option>
                            @foreach ($dosen as $row)
                         <option value="{{ $row->id }}">{{ $row->nama_dosen }}</option>
                         @endforeach
                        </select>
                      </div>

                  </div>
                </div>

                <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                  <button type="submit" style="color:blue">
                    <strong>SAVE</strong>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>




</x-app-layout>

