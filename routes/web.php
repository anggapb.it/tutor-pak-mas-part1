<?php

use App\Http\Controllers\PengajuanSkripsiController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::middleware('auth')->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
    Route::get('/pengajuan-skripsi', [PengajuanSkripsiController::class, 'index'])->name('pengajuan-skripsi');
    Route::get('/pengajuan-skripsi-create', [PengajuanSkripsiController::class, 'create'])->name('pengajuan-skripsi-create');
    Route::post('/pengajuan-skripsi-store', [PengajuanSkripsiController::class, 'saveData'])->name('pengajuan-skripsi-store');
    Route::get('/pengajuan-skripsi-delete/{id}', [PengajuanSkripsiController::class, 'deleteData'])->name('pengajuan-skripsi-delete');
});

require __DIR__.'/auth.php';
