<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Skripsi extends Model
{
    use HasFactory;

    protected $table = 'skripsi';

    protected $guarded = [];

    public function dosenWali()
    {
        return $this->hasOne(Dosen::class, 'id', 'dosenwali_id');
    }

    public function getDosenKonsulSatu()
    {
        return $this->hasOne(Dosen::class, 'id', 'doskonsul1_id');
    }

    public function getDosenKonsulDua()
    {
        return $this->hasOne(Dosen::class, 'id', 'doskonsul2_id');
    }

    public function getDosPemSatu()
    {
        return $this->hasOne(Dosen::class, 'id', 'dospem1_id');
    }

    public function getDosPemDua()
    {
        return $this->hasOne(Dosen::class, 'id', 'dospem2_id');
    }

    public function getMahasiswa()
    {
        return $this->hasOne(Mahasiswa::class, 'id', 'mahasiswa_id');
    }
}
