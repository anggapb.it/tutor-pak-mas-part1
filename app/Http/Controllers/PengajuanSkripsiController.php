<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\{Dosen,Mahasiswa};
use App\Models\Skripsi;
use Response;

class PengajuanSkripsiController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $data = Skripsi::with('dosenWali','getDosenKonsulSatu','getDosenKonsulDua','getDosPemSatu','getDosPemDua','getMahasiswa')->get();
        return view('skripsi/index',compact('data'));
    }

    public function create()
    {
        $mahasiswa = Mahasiswa::all();
        $dosen = Dosen::all();
        return view('skripsi/form', compact('mahasiswa','dosen'));
    }


    public function saveData(Request $request)
    {
        $arr   =   Skripsi::create(
            [
            'user_id' => 1,
            'mahasiswa_id' => $request->mahasiswa_id,
            'dosenwali_id' => $request->dosenwali_id,
            'doskonsul1_id' => $request->dosenkonsul1_id,
            'doskonsul2_id' => $request->dosenkonsul2_id,
            'dospem1_id' => $request->dospem1_id,
            'dospem2_id' => $request->dospem2_id,
            'keahlian' => $request->keahlian,
            'ipk' => $request->ipk,
            'judul_skripsi' => $request->judul_skripsi,
            ]
          );

          return redirect('/pengajuan-skripsi');
    }

}
