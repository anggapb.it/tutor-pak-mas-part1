<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkripsiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skripsi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('mahasiswa_id');
            $table->integer('dosenwali_id');
            $table->integer('doskonsul1_id');
            $table->integer('doskonsul2_id');
            $table->integer('dospem1_id');
            $table->integer('dospem2_id');
            $table->string('keahlian');
            $table->string('ipk');
            $table->string('judul_skripsi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skripsi');
    }
}
